using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    class FileLogger: ILogger
    {
        private string filePath;

        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }

        public void Log(string message) //četvrti zadatak
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
            writer.WriteLine(message);
        }
        }

        //peti
        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter writer=new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }
    }
}