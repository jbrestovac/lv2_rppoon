using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    interface IHandleDice { 
        void RemoveAllDice(); 
        void RollAllDice();
    }

}