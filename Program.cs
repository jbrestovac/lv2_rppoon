using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPOON_LV2_Brestovac
{
    class Program
    {   
        
        static void Main(string[] args)
        {   //Prvi
            Die die = new Die(6);

            DiceRoller diceRoller = new DiceRoller();


            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            diceRoller.PrintRollingResults();

            //Drugi
            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random();
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6, randomGenerator));
            }
            diceRoller.RollAllDice();
            diceRoller.PrintRollingResults();

            //Treci
            DiceRoller diceRoller = new DiceRoller();


            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            diceRoller.PrintRollingResults();

            //Cetvrti

            DiceRoller diceRoller = new DiceRoller();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            diceRoller.SetLogger(consoleLogger);
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            diceRoller.LogRollingResult();


            //Peti
            DiceRoller diceRoller = new DiceRoller();
            ConsoleLogger consoleLoger = new ConsoleLogger();
            diceRoller.SetLogger(consoleLogger);
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();
            consoleLogger.Log(diceRoller);

            //šesti

            FlexibleDiceRoller diceRoller = new FlexibleDiceRoller();
            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }

            diceRoller.RollAllDice();
            diceRoller.PrintRollingResults();
            diceRoller.RemoveAllDice();
            diceRoller.PrintRollingResults();

            //sedmi

            FlexibleDiceRoller diceRoller = new FlexibleDiceRoller();
            for (int i = 0; i < 10; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            for (int i = 0; i < 10; i++)
            {
                diceRoller.InsertDie(new Die(15));
            }
            diceRoller.RollAllDice();
            diceRoller.PrintRollingResults();
            diceRoller.RemoveDieForSides(12);
            diceRoller.RollAllDice();
            Console.WriteLine("Ponovljeno bacanje:\n");
            diceRoller.PrintRollingResults();




        }



    }
}