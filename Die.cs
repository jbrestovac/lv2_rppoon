using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;

        //drugi zadatak:
        public Die(int numberOfSides, Random randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance(); //treći zadatak
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1); //treći zadatak
            return rolledNumber;
        }

        //sedmi
        public int NumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }
}