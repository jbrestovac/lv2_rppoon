using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    interface IDiceRoller { 
        void InsertDie(); 
        void RemoveAllDice(); 
        void RollAllDice(); }

}
