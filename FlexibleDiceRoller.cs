using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    //šesti
    class FlexibleDiceRoller: IRoller, IHandleDice
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //sedmi
        public void RemoveDieForSides(int sides)
        {
            for(int i=0; i<dice.Count; i++)
            {
                if (dice[i].NumberOfSides == sides)
                {
                    dice.RemoveAt(i);
                    --i;
                }
            }
        }
    }
}
