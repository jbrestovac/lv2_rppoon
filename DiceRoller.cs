using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2_Brestovac
{
    class DiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger; //četvrti zadatak
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public virtual void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void PrintRollingResults()
        {
            IList<int> results = diceRoller.GetRollingResults();
            foreach (int result in results)
            {
                Console.WriteLine(result);
            }
        }
        public virtual void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }

        //četvrti
        public void SetLogger(ILogger logger)
        {
            this.logger = logger;
        }

        public void LogRollingResults()
        {
            
            foreach(int result in this.resultForEachRoll)
            {
                logger.Log(result.ToString());
            }
        }

        //peti
        public virtual void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
            
        }

        public String GetStringRepresentation()
        {
            StringBuilder stringBuilder= new StringBuilder();
            foreach(int result in this.resultForEachRoll)
            {
                stringBuilder.Append(result.ToString()).Append("\n");

            }

            return stringBuilder.ToString(); 
        }
    }

}
